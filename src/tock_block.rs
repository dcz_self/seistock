use libtock2::block_storage as driver;

use seismos::storage;
use seismos::storage::{ PageIdx, BlockIdx };

use seismos::storage::{ Device, Page };

/// Synchronous
pub struct BlockStorage<const BPD: usize, const PPB: usize, const BPP: usize>(());

impl<const BPP: usize, const PPB: usize, const BPD: usize> BlockStorage<BPD, PPB, BPP> {
    pub fn new() -> Result<Self, &'static str> {
        if driver::BlockStorage::driver_check() {
            let g = driver::BlockStorage::get_geometry();
            dbg!(&g, BPP, BPP*PPB);
            if g.write_block_size != BPP as u32 {
                Err("Write size")
            } else if g.erase_block_size as usize != BPP * PPB {
                Err("Erase size")
            } else if driver::BlockStorage::get_size() as usize != BPP * PPB * BPD {
                Err("Total size")
            } else {
                Ok(Self(()))
            }
        } else {
            Err("Device")
        }
    }
}

impl<const BPP: usize, const PPB: usize, const BPD: usize> Device for BlockStorage<BPD, PPB, BPP> {
    type Page = storage::P<BPP>;
    type Geometry = storage::FlatGeom<BPD, PPB, BPP>;

    fn read_page(&self, index: PageIdx<Self::Geometry>, data: &mut Self::Page) {
        let byte_offset = (index.0.0 * PPB + index.1) * BPP;
        dbg!("read", byte_offset);
        driver::BlockStorage::read(
            (index.0.0 * PPB + index.1) as u32,
            data.get_data_mut(),
        ).unwrap();
    }
    fn write_page(&mut self, index: PageIdx<Self::Geometry>, data: &Self::Page) {
        let byte_offset = (index.0.0 * PPB + index.1) * BPP;
        driver::BlockStorage::write(
            (index.0.0 * PPB + index.1) as u32,
            data.get_data(),
        ).unwrap();
    }
    fn erase_block(&mut self, index: BlockIdx<Self::Geometry>) {
        driver::BlockStorage::erase(index.0 as u32).unwrap()
    }
}