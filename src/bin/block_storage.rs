//! An extremely simple libtock-rs example. Just prints out a message
//! using the Console capsule, then terminates.

#![no_main]
#![no_std]
use core::fmt::Write;
use libtock2::console::Console;
use libtock2::runtime::{set_main, stack_size};
use seistock::tock_block::BlockStorage;
use seismos::{Seismos, FileId, SheetId};

set_main! {main}
stack_size! {0x1800}

fn main() {
    let mut w = Console::writer();
    let mut fs: Seismos<_, seismos::checksum::Crc32>
        = Seismos::new(BlockStorage::<2048, 16, 256>::new().unwrap());

    writeln!(&mut w, "FS OK").unwrap();
    
    let mut offset = 0;
    let mut found = false;
    fs.read_from_sheet(
        SheetId(FileId(1), 0),
        &mut |buf| {
            write!(&mut w, ".").unwrap();
            offset += buf.len();
            found = true;
            true
        },
    );
    if found {
        writeln!(&mut w, "").unwrap();
        writeln!(&mut w, "Found {} bytes", offset).unwrap();
    } else {
        writeln!(&mut w, "No file, all clear").unwrap();
    }
}
