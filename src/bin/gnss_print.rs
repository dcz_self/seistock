//! An extremely simple libtock-rs example. Just prints out a message
//! using the Console capsule, then terminates.

#![no_main]
#![no_std]
use core::fmt::Write;
use libtock2::console::Console;
use libtock2::gnss::GNSS;
use libtock2::runtime::{set_main, stack_size};
use seistock::println;

set_main! {main}
stack_size! {0x1800}

fn main() {
    let mut w = Console::writer();

    if !GNSS::driver_check() {
        println!("NO GNSS");
    } else {
        writeln!(&mut w, "GNSS START").unwrap();
        let mut buf = [0; 64];
        loop {
            let (read_count, ret) = GNSS::read(&mut buf);
            Console::write_all(&buf[..read_count as usize]).unwrap();
            if let Err(e) = ret {
                println!("\nERR {:?}", e);
            }
        }
    }
}
