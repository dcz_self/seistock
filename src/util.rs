#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => (
        use libtock2::console::Console;
        use core::fmt::Write;
        write!(&mut Console::writer(), $($arg)*).unwrap()
    );
}

#[macro_export]
macro_rules! println {
    ($($arg:tt)*) => (
        use libtock2::console::Console;
        use core::fmt::Write;
        writeln!(&mut Console::writer(), $($arg)*).unwrap()
    );
}

#[macro_export]
/// Prints out the expression, then returns it.
macro_rules! dbg {
    // NOTE: We cannot use `concat!` to make a static string as a format argument
    // of `eprintln!` because `file!` could contain a `{` or
    // `$val` expression could be a block (`{ .. }`), in which case the `eprintln!`
    // will be malformed.
    () => {
        $crate::println!("[{}:{}]", file!(), line!())
    };
    ($val:expr $(,)?) => {
        // Use of `match` here is intentional because it affects the lifetimes
        // of temporaries - https://stackoverflow.com/a/48732525/1063961
        match $val {
            tmp => {
                $crate::println!("[{}:{}] {} = {:#?}",
                    file!(), line!(), stringify!($val), &tmp);
                tmp
            }
        }
    };
    ($($val:expr),+ $(,)?) => {
        ($($crate::dbg!($val)),+,)
    };
}