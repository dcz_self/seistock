reqs:
    cargo install --path ../lbtck-rs/runner/ --bin runner

build:
    LIBTOCK_ARCH=cortex-m4 LIBTOCK_PLATFORM=nrf52840 cargo build --target=thumbv7em-none-eabi --release

build_bin NAME: build
    LIBTOCK_ARCH=cortex-m4 runner target/thumbv7em-none-eabi/release/{{NAME}}

build_tab NAME:
    @just build_bin {{NAME}}
    elf2tab /target/thumbv7em-none-eabi/release/cortex-m4.{{NAME}}.elf --kernel-major 2